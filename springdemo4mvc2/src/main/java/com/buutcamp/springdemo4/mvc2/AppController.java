package com.buutcamp.springdemo4.mvc2;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AppController {


    @RequestMapping
    public String frontPageGET(Model model) {


        return "front-page";
    }
}
